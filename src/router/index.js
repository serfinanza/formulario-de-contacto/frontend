import {createRouter, createWebHistory, RouterView} from 'vue-router'


const routes = [

    {
        path    : '/',
        redirect: {name: 'contact'}
    },

    {
        path     : '/contact',
        name     : 'contact',
        component: () => import('../views/contact'),
    },
    
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
